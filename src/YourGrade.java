import java.util.Scanner;
public class YourGrade {
    public static void main(String[] args) throws Exception {
        Scanner strScore = new Scanner(System.in);
        System.out.print("Please Input your score:");
        int score = strScore.nextInt();
        System.out.print("Grade:");
        if (score>=80){
            System.out.println("A");
        }else if(score<80&&score>=75){
            System.out.println("B+");
        }else if(score<75&&score>=70){
            System.out.println("B");
        }else if(score<70&&score>=65){
            System.out.println("C+");
        }else if(score<65&&score>=60){
            System.out.println("C");
        }else if(score<60&&score>=55){
            System.out.println("D+");
        }else if(score<55&&score>=50){
            System.out.println("D");
        }else if(score<50){
            System.out.println("F");
        }else{
            System.out.println("test1");
        }
    }
}
